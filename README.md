# Marvel API Client written in Java#

This API consists of capturing information from Marvel Api (https://developer.marvel.com/) as well as the processing of this information for data consumption.

This library is implemented using Java 7

**IMPORTANT**: This library is under development.

## Usage ##

Prerequisites: You need to obtain valid Marvel keys for using this library. You can obtain it from Marvel (https://developer.marvel.com/) 

**Implementation Notes** (*Commic *)

Fetches lists of comics containing a specific character, with optional filters. See notes on individual parameters below

**Comic** (Main)

Get all Comics data, use endpoint: http://localhost:8080/comic/getApi

Get all Comics data with All Character, use endpoint: http://localhost:8080/comic/comicFromCharacter

Get all Comics data for the specific Character, use endpoint: http://localhost:8080/comic/comicFromCharacter/<ID>
    Note: <ID> = id of the character

Get all Comics data of the database, use endpoint: http://localhost:8080/comic

Get Comics data of the database for the specific id, use endpoint: http://localhost:8080/comic/<ID>
    Note: <ID> = id of the comic

Get Comics data of the database for the specific id and Description, use endpoint: http://localhost:8080/comic
    Note: this query must be done by passing a JSON Comic object in the Request body

SAVE Comics data, use endpoint: http://localhost:8080/comic
    Note: This query must be done by passing a JSON Comic object in the Request body; using the POST

UPDATE Comics data, use endpoint: http://localhost:8080/comic
    Note: This query must be done by passing a JSON Comic object in the Request body; using the PUT

DELETE Comics data, use endpoint: http://localhost:8080/comic
    Note: <ID> = id of the comic

OTHER's:

**Character** 

Get character data use endpoint http://localhost:8080/character

**Series** 

Get comic data use endpoint http://localhost:8080/series

**Stack** 

    Spring Boot
    Spring MVC 4
    JAX-RS com Jersey
    Jackson
    Gson
    JUnit
    Tomcat

Base Project: https://bitbucket.org/andremp_cit/hands-on