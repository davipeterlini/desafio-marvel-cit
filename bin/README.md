# Marvel API Client written in Java#

This API consists of capturing information from Marvel Api (https://developer.marvel.com/) as well as the processing of this information for data consumption.

This library is implemented using Java 7

**IMPORTANT**: This library is under development.

## Usage ##

Prerequisites: You need to obtain valid Marvel keys for using this library. You can obtain it from Marvel (https://developer.marvel.com/) 

**Getting  Data** (*Chracter List*)

To obtain the data just carry out a query in the http://localhost:8080/desafio-marvel-cit/gettingDataMarvel/characterList endpoint

**Character** 

Get character data use endpoint http://localhost:8080/character

**Comic** 

Get comic data use endpoint http://localhost:8080/comic

**Series** 

Get comic data use endpoint http://localhost:8080/series

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact