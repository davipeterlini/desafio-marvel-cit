package br.com.treinamento.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "HealthCheck")
@javax.annotation.Generated(value = "ComicController")
@Path("/checkJersey")
public class HealthCheckJersey {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Teste", notes = "Teste", response = String.class, responseContainer = "List")
	public String test() throws Exception {
		return "Jersey - OK";
	}
}