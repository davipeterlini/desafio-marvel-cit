package br.com.treinamento.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.treinamento.service.CharacterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "CharacterController")
@javax.annotation.Generated(value = "CharacterController")
@Path("/character")
public class CharacterController {

	@Autowired
	private CharacterService characterService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "List Characters - Marvel", notes = "List Characters - Marvel", response = Character.class, responseContainer = "List")
	public List<br.com.treinamento.model.Character> getAllServiceOrderSubtype() throws Exception {
		return characterService.getAllCharacter();
	}
}