package br.com.treinamento.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.treinamento.model.Comic;
import br.com.treinamento.service.ComicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "ComicController")
@javax.annotation.Generated(value = "ComicController")
@Path("/comic")
public class ComicController {

	@Autowired
	private ComicService comicService;

	private final String ERROR_MSG = "Ocorreu um erro, Verifique os dados";

	// Lista de Quadrinhos da Marvel
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getApi")
	@ApiOperation(value = "List Comics - Marvel", notes = "List Comics - Marvel", response = Comic.class, responseContainer = "List")
	public List<Comic> getAllComicApi() throws Exception {
		return comicService.getAllComicApi();
	}

	// Lista de Quadrinhos de Todas as Personagens da Marvel
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/comicFromCharacter")
	@ApiOperation(value = "List Comics for All Character - Marvel", notes = "List Comics for All Character - Marvel", response = Comic.class, responseContainer = "List")
	public List<Comic> getMarvelComicsByCharacter() throws Exception {
		return comicService.getMarvelComicsByCharacter();
	}

	// Lista de Quadrinhos de um personagem
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/comicFromCharacter/{id}")
	@ApiOperation(value = "List Comics for Character - Marvel", notes = "List Comics for Character - Marvel", response = Comic.class, responseContainer = "List")
	public List<Comic> getAllComicFromCharacter(@PathVariable Long id) {
		return comicService.getAllComicFromCharacter(id);
	}

	// OPERAÇÕES EM MEMÓRIA

	// Lista de Quadrinhos do Banco
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get Comic", notes = "Get Comic", response = Comic.class, responseContainer = "")
	public List<Comic> getAllComic() {
		return comicService.getAllComic();
	}

	// Quadrinho
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	@ApiOperation(value = "Get Comic By Id", notes = "Get Comic By Id", response = Comic.class, responseContainer = "")
	public Comic getById(@PathVariable Long id) {
		return comicService.getById(id);
	}

	// Quadrinho por Descrição e ID
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/byDescription")
	@ApiOperation(value = "Get Comic By Id And Name", notes = "Get Comic By Id And Name", response = Comic.class, responseContainer = "")
	public Comic getByIdAndName(@RequestParam Comic comic) {
		return comicService.getByIdAndName(comic.getId(), comic.getDescription());
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Save Comic", notes = "Save Comic", response = Comic.class, responseContainer = "")
	public String save(@RequestBody Comic comic) throws Exception {
		try {
			comicService.save(comic);
		} catch (Exception e) {
			throw new Exception(ERROR_MSG + e);
		}
		return "O item " + comic.getDescription() + " foi atualizado com sucesso";
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Update Comic", notes = "Update Comic", response = Comic.class, responseContainer = "")
	public String update(@RequestBody Comic comic) throws Exception {
		try {
			comicService.update(comic);
		} catch (Exception e) {
			throw new Exception(ERROR_MSG + e);
		}
		return "O item " + comic.getDescription() + " foi atualizado com sucesso";
	}

	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "Delete Comic", notes = "Delete Comic", response = Comic.class, responseContainer = "")
	public String delete(@PathParam("id") Long id) throws Exception {
		try {
			comicService.delete(id);
		} catch (Exception e) {
			throw new Exception(ERROR_MSG + e);
		}
		return "O item " + id + " foi removido";
	}
}