package br.com.treinamento.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.treinamento.model.Series;
import br.com.treinamento.service.SeriesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "SeriesController")
@javax.annotation.Generated(value = "SeriesController")
@Path("/series")
public class SeriesController {

	@Autowired
	private SeriesService seriesService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "List Series - Marvel", notes = "List Series - Marvel", response = Series.class, responseContainer = "List")
	public List<Series> getAllServiceOrderSubtype() throws Exception {
		return seriesService.getAllService();
	}
}