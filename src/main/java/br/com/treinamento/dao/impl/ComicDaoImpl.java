package br.com.treinamento.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.treinamento.dao.ComicDao;
import br.com.treinamento.model.Comic;

@Transactional
@Repository
public class ComicDaoImpl implements ComicDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Comic> getAll() {
		TypedQuery<Comic> query = em.createQuery("select e from Comic e", Comic.class);
		return query.getResultList();
	}

	@Override
	public Comic getById(Long id) {
		TypedQuery<Comic> query = em.createQuery("select obj from CharacterList obj where obj.id = :id", Comic.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	@Override
	public Comic getByIdAndName(Long id, String name) {
		TypedQuery<Comic> query = em
				.createQuery("select obj from CharacterList obj where obj.id = :id and obj.name = :name", Comic.class);
		query.setParameter("id", id);
		query.setParameter("name", name);
		return query.getSingleResult();
	}

	// Save
	@Override
	public Comic save(Comic obj) {
		em.persist(obj);
		return obj;
	}

	// Update
	@Override
	public Comic update(Comic obj) {
		em.merge(obj);
		return obj;
	}

	// Delete
	@Override
	public Comic delete(Comic obj) {
		em.remove(obj);
		return obj;
	}
}