package br.com.treinamento.dao;

import java.util.List;

import br.com.treinamento.model.Comic;

public interface ComicDao {

	public List<Comic> getAll();

	public Comic getById(Long id);

	public Comic getByIdAndName(Long id, String name);

	public Comic save(Comic obj);

	public Comic update(Comic obj);

	public Comic delete(Comic obj);
}