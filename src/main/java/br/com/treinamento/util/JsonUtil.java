package br.com.treinamento.util;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class JsonUtil {

	public static Gson getGson() {
		final GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.disableHtmlEscaping().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
				.serializeNulls().create();
		return gson;
	}

	public static <T> T deserializeObject(Class<T> clazz, String jsonString)
			throws JsonProcessingException, IOException, InstantiationException, IllegalAccessException {
		Gson gson = getGson();
		return gson.fromJson(jsonString, clazz);
	}

	public static <T> T deserializeObject(Class<T> clazz, String jsonString, ObjectMapper mapper)
			throws JsonProcessingException, IOException, InstantiationException, IllegalAccessException {
		return mapper.readValue(jsonString, clazz);
	}

	public static <T> List<T> deserializeObject(Type listType, String jsonString)
			throws JsonProcessingException, IOException, InstantiationException, IllegalAccessException {
		Gson gson = getGson();
		return gson.fromJson(jsonString, listType);
	}

	public static <T> String parseToString(T object) {
		return getGson().toJson(object);
	}
}