package br.com.treinamento.marvel;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;

import br.com.treinamento.config.MarvelRestClientConfig;
import br.com.treinamento.enums.EnumTypeMarvelApi;

public class GetJSONService {

	@Value("${marvelapi.public.key}")
	private String publicKey;

	@Value("${marvelapi.api.private.key}")
	private String privateKey;

	private static String PUBLIC_KEY = "04c60a3997720cb8b6d4123bd7a31bc2";
	private static String PRIVATE_KEY = "50a10d35f10825fd21a627579cdee8327d8fff37";

	public static JSONArray getJSONArray(EnumTypeMarvelApi enumTypeMarvelApi, String type, Long id)
			throws JSONException {
		JSONObject jsonObject = new JSONObject(getJSON(enumTypeMarvelApi, type, id));
		JSONObject myResponse = jsonObject.getJSONObject("data");
		JSONArray tsmresponse = (JSONArray) myResponse.get("results");
		return tsmresponse;
	}

	// Obtendo Objetos da Requisção - JSON (Character)
	private static String getJSON(EnumTypeMarvelApi enumTypeMarvelApi, String type, Long id) {
		String url = MarvelRestClientConfig.mountRestUrl(PRIVATE_KEY, PUBLIC_KEY, enumTypeMarvelApi);
		if (type != null) {
			url.replace("characters",
					enumTypeMarvelApi.toString().toLowerCase() + "/" + String.valueOf(id) + "/" + type);
		}
		return ClientBuilder.newClient().target(url).request().accept(MediaType.APPLICATION_JSON).get(String.class);
	}

}
