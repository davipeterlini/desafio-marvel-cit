package br.com.treinamento.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import br.com.treinamento.controller.CharacterController;
import br.com.treinamento.controller.ComicController;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;

@Component
//@Configuration
public class JerseyConfig extends ResourceConfig {
	
	public JerseyConfig() {
        registerEndpoints();
        configureSwagger();
	}

    private void configureSwagger() {
        register(ApiListingResource.class);
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8080");
        beanConfig.setBasePath("/");
        beanConfig.setResourcePackage("your.resource.package.here,your.other.package.here");
        beanConfig.setPrettyPrint(true);
        beanConfig.setScan(true);
    }

    private void registerEndpoints() {
        register(CharacterController.class);
        register(ComicController.class);
		//packages("br.com.treinamento.controller");
    }
}