package br.com.treinamento.config;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import br.com.treinamento.enums.EnumTypeMarvelApi;

public class MarvelRestClientConfig {

	public static String mountRestUrl(String privateKey, String apikey, EnumTypeMarvelApi type) {
		Long timeStamp = new Date().getTime();
		String hash = MarvelRestClientConfig.generateHash(timeStamp + privateKey + apikey);
		String urlCompleta = null;

		if (EnumTypeMarvelApi.COMICS.equals(type)) {
			urlCompleta = "http://gateway.marvel.com/v1/public/comics?ts=" + timeStamp + "&apikey=" + apikey + "&hash="
					+ hash;
		} else if (EnumTypeMarvelApi.CHARACTERS.equals(type)) {
			urlCompleta = "http://gateway.marvel.com/v1/public/characters?ts=" + timeStamp + "&apikey=" + apikey
					+ "&hash=" + hash;
		} else if (EnumTypeMarvelApi.SERIES.equals(type)) {
			urlCompleta = "http://gateway.marvel.com/v1/public/series?ts=" + timeStamp + "&apikey=" + apikey + "&hash="
					+ hash;
		}
		return urlCompleta;
	}

	public static String generateHash(String string) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		md.update(string.getBytes());
		byte[] bytes = md.digest();

		StringBuilder s = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			int parteAlta = ((bytes[i] >> 4) & 0xf) << 4;
			int parteBaixa = bytes[i] & 0xf;
			if (parteAlta == 0) {
				s.append('0');
			}
			s.append(Integer.toHexString(parteAlta | parteBaixa));
		}
		return s.toString();
	}
}