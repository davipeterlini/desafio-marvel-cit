package br.com.treinamento.service;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.treinamento.model.Comic;

@Service
public interface ComicService {

	public List<Comic> getAllComicApi()
			throws JsonProcessingException, InstantiationException, IllegalAccessException, IOException, JSONException;

	public List<Comic> getAllComicFromCharacter(Long idFromCharacter);

	public List<Comic> getMarvelComicsByCharacter()
			throws JsonProcessingException, InstantiationException, IllegalAccessException, JSONException, IOException;

	public List<Comic> getAllComic();

	public Comic getById(Long id);

	public Comic getByIdAndName(Long id, String name);

	public void save(Comic comic);

	public void update(Comic comic);

	public void delete(Long id);

}