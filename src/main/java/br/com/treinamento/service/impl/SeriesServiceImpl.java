package br.com.treinamento.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.treinamento.enums.EnumTypeMarvelApi;
import br.com.treinamento.marvel.GetJSONService;
import br.com.treinamento.model.Series;
import br.com.treinamento.service.SeriesService;

@Service
public class SeriesServiceImpl implements SeriesService {

	public List<Series> getAllService()
			throws JsonProcessingException, InstantiationException, IllegalAccessException, IOException, JSONException {

		JSONArray tsmresponse = GetJSONService.getJSONArray(EnumTypeMarvelApi.SERIES, null, null);

		ArrayList<HashMap<String, String>> prodArrayList = new ArrayList<HashMap<String, String>>();

		for (int i = 0; i < tsmresponse.length(); i++) {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			hashMap.put("id", String.valueOf(tsmresponse.getJSONObject(i).getLong("id")));
			hashMap.put("title", tsmresponse.getJSONObject(i).optString("title"));
			hashMap.put("description", tsmresponse.getJSONObject(i).optString("description"));
			hashMap.put("resourceURI", tsmresponse.getJSONObject(i).optString("resourceURI"));
			hashMap.put("startYear", String.valueOf(tsmresponse.getJSONObject(i).getInt("startYear")));
			hashMap.put("endYear", String.valueOf(tsmresponse.getJSONObject(i).getInt("endYear")));
			hashMap.put("rating", tsmresponse.getJSONObject(i).optString("rating"));
			hashMap.put("modified", tsmresponse.getJSONObject(i).optString("modified"));
			prodArrayList.add(hashMap);
		}

		List<Series> series = new ArrayList<Series>();

		for (HashMap<String, String> result : prodArrayList) {
			Series serie = new Series();
			serie.setId(Long.valueOf(result.get("id")));
			serie.setTitle(result.get("title"));
			serie.setDescription(result.get("description"));
			serie.setResourceURI(result.get("resourceURI"));
			serie.setStartYear(Integer.parseInt(result.get("startYear")));
			serie.setEndYear(Integer.parseInt(result.get("endYear")));
			serie.setRating(result.get("rating"));
			serie.setModified(result.get("modified"));
			series.add(serie);
		}
		return series;
	}
}