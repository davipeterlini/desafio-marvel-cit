package br.com.treinamento.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.treinamento.enums.EnumTypeMarvelApi;
import br.com.treinamento.marvel.GetJSONService;
import br.com.treinamento.model.Character;
import br.com.treinamento.service.CharacterService;

@Service
public class CharacterServiceImpl implements CharacterService {

	public List<Character> getAllCharacter()
			throws JsonProcessingException, InstantiationException, IllegalAccessException, IOException, JSONException {
		ArrayList<HashMap<String, String>> prodArrayList = new ArrayList<HashMap<String, String>>();

		JSONArray tsmresponse = GetJSONService.getJSONArray(EnumTypeMarvelApi.CHARACTERS, null, null);

		for (int i = 0; i < tsmresponse.length(); i++) {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			hashMap.put("id", String.valueOf(tsmresponse.getJSONObject(i).getLong("id")));
			hashMap.put("name", tsmresponse.getJSONObject(i).optString("name"));
			hashMap.put("description", tsmresponse.getJSONObject(i).optString("description"));
			hashMap.put("modified", tsmresponse.getJSONObject(i).optString("modified"));
			hashMap.put("resourceURI", tsmresponse.getJSONObject(i).optString("resourceURI"));
			prodArrayList.add(hashMap);
		}

		List<Character> characters = new ArrayList<Character>();

		for (HashMap<String, String> result : prodArrayList) {
			Character character = new Character();
			character.setId(Long.valueOf(result.get("id")));
			character.setName(result.get("name"));
			character.setDescription(result.get("description"));
			character.setModified(result.get("modified"));
			character.setResourceUri(result.get("resourceURI"));
			characters.add(character);
		}
		return characters;
	}
}