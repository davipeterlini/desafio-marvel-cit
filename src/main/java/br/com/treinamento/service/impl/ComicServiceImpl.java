package br.com.treinamento.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.treinamento.dao.ComicDao;
import br.com.treinamento.enums.EnumTypeMarvelApi;
import br.com.treinamento.marvel.GetJSONService;
import br.com.treinamento.model.Comic;
import br.com.treinamento.service.CharacterService;
import br.com.treinamento.service.ComicService;

@Service
public class ComicServiceImpl implements ComicService {

	@Autowired
	private ComicDao comicDao;

	@Autowired
	private CharacterService characterService;

	@Override
	public List<Comic> getAllComicApi()
			throws JsonProcessingException, InstantiationException, IllegalAccessException, IOException, JSONException {
		List<Comic> comics = getComics(null, null);
		return comics;
	}
	
	public List<Comic> getAllComicFromCharacter(Long idFromCharacter) {
		List<Comic> comics = getComics(EnumTypeMarvelApi.COMICS, idFromCharacter);
		for (Comic comic : comics) {
			comicDao.save(comic);
		}
		return comics;
	}

	@Override
	public List<Comic> getMarvelComicsByCharacter()
			throws JsonProcessingException, InstantiationException, IllegalAccessException, JSONException, IOException {
		List<Comic> comicFromCharacter = new ArrayList<Comic>();
		List<br.com.treinamento.model.Character> characters = characterService.getAllCharacter();
		for (br.com.treinamento.model.Character character : characters) {
			comicFromCharacter = getAllComicFromCharacter(character.getId());
			for (Comic comic : comicFromCharacter) {
				comicDao.save(comic);
			}
		}
		return comicFromCharacter;
	}

	@Override
	public List<Comic> getAllComic() {
		List<Comic> resultComic = new ArrayList<Comic>();
		resultComic = comicDao.getAll();
		if (resultComic != null) {
			return resultComic;
		}
		return null;
	}

	@Override
	public Comic getById(Long id) {
		return comicDao.getById(id);
	}

	@Override
	public Comic getByIdAndName(Long id, String name) {
		return comicDao.getByIdAndName(id, name);
	}

	@Override
	public void save(Comic comic) {
		comicDao.save(comic);
	}

	@Override
	public void update(Comic comic) {
		if (comic.getId() != null) {
			comicDao.update(comic);
		}
	}

	@Override
	public void delete(Long id) {
		Comic comic = new Comic();
		comic.setId(id);
		comicDao.delete(comic);
	}

	private List<Comic> getComics(EnumTypeMarvelApi type, Long id) {
		ArrayList<HashMap<String, String>> prodArrayList = new ArrayList<HashMap<String, String>>();
		JSONArray tsmresponse = GetJSONService.getJSONArray(EnumTypeMarvelApi.COMICS, type.toString(), id);

		for (int i = 0; i < tsmresponse.length(); i++) {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			hashMap.put("id", String.valueOf(tsmresponse.getJSONObject(i).getLong("id")));
			hashMap.put("digitalId", String.valueOf(tsmresponse.getJSONObject(i).getInt("digitalId")));
			hashMap.put("title", tsmresponse.getJSONObject(i).optString("title"));
			hashMap.put("issueNumber", String.valueOf(tsmresponse.getJSONObject(i).getDouble("issueNumber")));
			hashMap.put("variantDescription", tsmresponse.getJSONObject(i).optString("variantDescription"));
			hashMap.put("description", tsmresponse.getJSONObject(i).optString("description"));
			hashMap.put("modified", tsmresponse.getJSONObject(i).optString("modified"));
			hashMap.put("isbn", tsmresponse.getJSONObject(i).optString("isbn"));
			hashMap.put("upc", tsmresponse.getJSONObject(i).optString("upc"));
			hashMap.put("diamondCode", tsmresponse.getJSONObject(i).optString("diamondCode"));
			hashMap.put("ean", tsmresponse.getJSONObject(i).optString("ean"));
			hashMap.put("issn", tsmresponse.getJSONObject(i).optString("issn"));
			hashMap.put("format", tsmresponse.getJSONObject(i).optString("format"));
			hashMap.put("pageCount", String.valueOf(tsmresponse.getJSONObject(i).getInt("pageCount")));

			prodArrayList.add(hashMap);
		}

		List<Comic> comics = new ArrayList<Comic>();

		for (HashMap<String, String> result : prodArrayList) {
			Comic comic = new Comic();
			comic.setId(Long.valueOf(result.get("id")));
			comic.setDigitalId(Integer.parseInt(result.get("digitalId")));
			comic.setTitle(result.get("title"));
			comic.setIssueNumber(Double.parseDouble(result.get("issueNumber")));
			comic.setVariantDescription(result.get("variantDescription"));
			comic.setDescription(result.get("description"));
			comic.setModified(result.get("modified"));
			comic.setIsbn(result.get("isbn"));
			comic.setUpc(result.get("upc"));
			comic.setDiamondCode(result.get("diamondCode"));
			comic.setEan(result.get("ean"));
			comic.setIssn(result.get("issn"));
			comic.setFormat(result.get("format"));
			comic.setPageCount(Integer.parseInt(result.get("pageCount")));
			comics.add(comic);
		}
		return comics;
	}
}