package br.com.treinamento.service;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.treinamento.model.Character;

@Service
public interface CharacterService {

	List<Character> getAllCharacter()
			throws JsonProcessingException, InstantiationException, IllegalAccessException, IOException, JSONException;
}