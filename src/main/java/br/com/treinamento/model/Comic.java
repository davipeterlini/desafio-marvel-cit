package br.com.treinamento.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "comic")
//@XmlRootElement(name = "comic")
public class Comic implements Serializable {

	private static final long serialVersionUID = -78799202361719686L;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
	//@SerializedName("id")
	private Long id;

    @Column(name = "digitalId")
	@SerializedName("digitalId")
	private int digitalId;

    @Column(name = "title", insertable = true, updatable = true)
	@SerializedName("title")
	private String title;

    @Column(name = "issueNumber", insertable = true, updatable = true)
	@SerializedName("issueNumber")
	private double issueNumber;

    @Column(name = "variantDescription", insertable = true, updatable = true)
	@SerializedName("variantDescription")
	private String variantDescription;

    @Column(name = "description", insertable = true, updatable = true)
	@SerializedName("description")
	private String description;

    @Column(name = "modified", insertable = true, updatable = true)
	@SerializedName("modified")
	private String modified;

    @Column(name = "isbn", insertable = true, updatable = true)
	@SerializedName("isbn")
	private String isbn;

    @Column(name = "upc", insertable = true, updatable = true)
	@SerializedName("upc")
	private String upc;

    @Column(name = "diamondCode", insertable = true, updatable = true)
	@SerializedName("diamondCode")
	private String diamondCode;

    @Column(name = "ean", insertable = true, updatable = true)
	@SerializedName("ean")
	private String ean;

    @Column(name = "issn", insertable = true, updatable = true)
	@SerializedName("issn")
	private String issn;

    @Column(name = "format", insertable = true, updatable = true)
	@SerializedName("format")
	private String format;

    @Column(name = "pageCount", insertable = true, updatable = true)
	@SerializedName("pageCount")
	private int pageCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getDigitalId() {
		return digitalId;
	}

	public void setDigitalId(int digitalId) {
		this.digitalId = digitalId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getIssueNumber() {
		return issueNumber;
	}

	public void setIssueNumber(double issueNumber) {
		this.issueNumber = issueNumber;
	}

	public String getVariantDescription() {
		return variantDescription;
	}

	public void setVariantDescription(String variantDescription) {
		this.variantDescription = variantDescription;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getDiamondCode() {
		return diamondCode;
	}

	public void setDiamondCode(String diamondCode) {
		this.diamondCode = diamondCode;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
}