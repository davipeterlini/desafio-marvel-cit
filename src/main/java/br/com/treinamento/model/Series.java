package br.com.treinamento.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.SerializedName;

@XmlRootElement(name = "series")
public class Series implements Serializable {

	private static final long serialVersionUID = -8799583833665410002L;

	@SerializedName("id")
	private Long id;

	@SerializedName("title")
	private String title;

	@SerializedName("description")
	private String description;

	@SerializedName("resourceURI")
	private String resourceURI;

	@SerializedName("startYear")
	private int startYear;

	@SerializedName("endYear")
	private int endYear;

	@SerializedName("rating")
	private String rating;

	@SerializedName("modified")
	private String modified;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getResourceURI() {
		return resourceURI;
	}

	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	public int getStartYear() {
		return startYear;
	}

	public void setStartYear(int startYear) {
		this.startYear = startYear;
	}

	public int getEndYear() {
		return endYear;
	}

	public void setEndYear(int endYear) {
		this.endYear = endYear;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}
}