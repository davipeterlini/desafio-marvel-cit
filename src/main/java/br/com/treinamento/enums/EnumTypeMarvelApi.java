package br.com.treinamento.enums;

public enum EnumTypeMarvelApi {

	CHARACTERS, COMICS, SERIES

}