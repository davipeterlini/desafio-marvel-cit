package br.com.treinamento.controller;

import java.util.Date;

import org.junit.Test;

import br.com.treinamento.config.MarvelRestClientConfig;

public class CharacterControllerTest {

	private static String PUBLIC_KEY = "04c60a3997720cb8b6d4123bd7a31bc2";
	private static String PRIVATE_KEY = "50a10d35f10825fd21a627579cdee8327d8fff37";

	@Test
	public void testMountURL() {
		System.out.println(mountRestUrl());
	}

	private static String mountRestUrl() {
		Long timeStamp = new Date().getTime();
		String hash = MarvelRestClientConfig.generateHash(timeStamp + PRIVATE_KEY + PUBLIC_KEY);

		String urlCompletaCharacter = "http://gateway.marvel.com/v1/public/characters?ts=" + timeStamp + "&apikey="
				+ PUBLIC_KEY + "&hash=" + hash;

		String urlCompletaComic = "http://gateway.marvel.com/v1/public/comics?ts=" + timeStamp + "&apikey=" + PUBLIC_KEY
				+ "&hash=" + hash;

		String urlCompletaSeries = "http://gateway.marvel.com/v1/public/series?ts=" + timeStamp + "&apikey="
				+ PUBLIC_KEY + "&hash=" + hash;

		System.out.println("Rest URL: " + urlCompletaCharacter);
		System.out.println("Rest URL: " + urlCompletaComic);
		System.out.println("Rest URL: " + urlCompletaSeries);

		return urlCompletaComic;
	}

}